'Tracks' - Location Tracking App Using React Native (as front-end) and Express server (with mongodb) as backend.

Backend Expresser server of this app is deployed on heroku by the URL:
https://safe-badlands-24499.herokuapp.com

And the repository of the server is available on my bitbucket profile by the name, 'tracker-app'

So there's no need to run the server separately on your machine, you can access and use this project directly.

The purpose of this React Native app is to
allow users to track locations for particular trips
on a map (such as for a bike ride, hanging out with others etc.)

Once the user signs up /logs in, the user is able to 'create a track':
Create Track Screen - allows user to start recording their location and a line gets drawn onto the map of user's instantaneous locations on the map in real time. The location gets tracked using expo-location. The user can also then stop recording the location and save it.

Once saved, the user gets directed to TrackListScreen - where they will be displayed with a list of their created tracks. By clicking on any particular track, the user gets directed to TrackDetail Screen - where they can view the map and line (drawn as coordinates (longitude/ latitude) of total location).

The user can also sign out from the app.

This project was creted using expo-cli. In order to run this project on your machine, make sure you have NodeJS installed.

Clone/download the repository. To install all the dependencies run:
npm install

Then run:
expo start

This will open the Metro Bundler on browser. If you have Expo application installed on your mobile, open the Expo application and scan the QR code (Tunnel version) from the Metro Bundler browser which will open the project on your mobile device.

Note: This project was actually built with the help of a Udemy course, and the project idea is by no means uniquely produced.
