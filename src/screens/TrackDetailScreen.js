import React, { useContext } from "react";
import { View, Text, StyleSheet } from "react-native";
import { Context as TrackContext } from "../context/TrackContext";
import MapView, { Polyline } from "react-native-maps";

const TrackDetailScreen = ({ navigation }) => {
  const { state } = useContext(TrackContext);

  const _id = navigation.getParam("_id");

  const track = state.find(t => t._id === _id);

  const initialCoords = track.locations[0].coords;

  return (
    <View>
      <Text
        style={{
          fontSize: 30,
          textAlign: "center",
          marginBottom: 20,
          marginTop: 5
        }}
      >
        {track.name}
      </Text>
      <MapView
        style={styles.marginOnTop}
        initialRegion={{
          longitudeDelta: 0.01,
          latitudeDelta: 0.01,
          ...initialCoords
        }}
        style={styles.map}
      >
        <Polyline coordinates={track.locations.map(loc => loc.coords)} />
      </MapView>
    </View>
  );
};

TrackDetailScreen.navigationOptions = {
  title: ""
};

const styles = StyleSheet.create({
  map: {
    height: 300
  },
  marginOnTop: {
    marginTop: 20
  }
});

export default TrackDetailScreen;
